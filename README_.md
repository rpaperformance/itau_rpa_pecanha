# Projeto RPA Lili

 O projeto RPA Lili é um "robô" responsável por realizar a integração bidirecional dos chamados de telefonia do portal Signus(Itaú) para SpreadUP(Spread) e efetuando atendimento de primeiro nível.

# Estrutura
 
 O projeto tem a estrutura baseada nas melhores praticas de PageLocators com a arvore do projeto na pasta sistema.
 
  - Signus_SpreadUp
 
    - log
  
    - sistema/pages
        - common_locators.rb
        - common_page.rb
        - email.rb
        - historico_locators.rb
        - historico_page.rb
        - menu_page.rb
        - registros_locators.rb
        - registros_page.rb
        - service_spreadUp.rb
           
    - support
        - env.rb
        - eventos_terminal.rb
        - excel.rb
        - manipula_excel.rb
        - support.rb
    
    - .gitignore
    - Gemfile
    - main.rb
    - README.MD
    - run.bat
        

# Fluxos da Integração

A integração está dividida em duas partes sendo elas

1 - Extração dos chamados do Signus e upload para SpreadUp
2 - Atualização e transferência dos chamados do Signus

#Estrutura

...
>>>>>>> cde0830ab2b0bfc922c9a2d9876d790c061c0941
