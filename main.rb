﻿#encoding: utf-8
####################################################################
# PROJETO.: RPA - PEÇANHA
# AUTOR...: DOUGLAS PASSOS SANTOS
# EMPRESA.: SPREAD
####################################################################
require "./sistema/support/env"

common = CommonPage.new
menu = MenuPage.new
historico = HistoricoPage.new
email = Email.new
api_spreadup = Servico_spreadup.new
#data_base = Data_base.new

link = "http://hspportal.itau:8084/fullscreen"
sistema = "signus"
transacao = "d3ca"
$chamados = Array.new
$dado_chamado = Hash.new
$lista_email_from = Configuracao["lista_email_from"]
$lista_email_to = Configuracao["lista_email_to"]


begin
  #######################################################################################
  # RETORNA LISTAS DE CHAMADOS PARA MONITORAR
  #######################################################################################
  begin
    $chamados = Array.new
    #$chamados = ["P6014836"] 
    #$chamados = ["P6270507"] 
    $chamados = api_spreadup.obter_lista_chamados_spreadUp
    raise "01 - NAO HA CHAMADOS" if $chamados == "SEM CHAMADO SPREADUP"
    total_chamados = $chamados.size
    $log.info "##########################################################################"
    $log.info "# INCIADO - TOTAL DE CHAMADOS #{total_chamados} - DATA HORA #{Time.now.strftime('%H:%M:%S')}"
    $log.info "##########################################################################"    
  rescue => ex
    retry if ex.message.include? "01 - NAO HA CHAMADOS" and sleep 100
    raise "101 - FALHA AO OBTER CHAMADOS DO SPREADUP"
  end
  #######################################################################################
  # Acessa signus
  #######################################################################################
  $log.info "###### ACESSA SIGNUS ######"
  begin
    # HORARIO DO SIGNUS EM MANUTENÇÃO - 20:00 ATÉ AS 20:20
    while Time.now.strftime('%H:%M') < '20:20' do sleep 1 end if Time.now.strftime('%H:%M') > '19:59' and Time.now.strftime('%H:%M') < '20:20'

    common.acessar_signus(link, sistema)
    user = Configuracao["user"]
    pass = Configuracao["password"]
    if common.fazer_login(user, pass) != "SUCESSO"
      raise "FALHA NO LOGIN SIGNUS - USER: #{user} PASS: #{pass}"
    end
    common.acessar_transacao(transacao)
    menu.acessar_menu
    menu.acessar_gerenciamento_de_ocorrencias
    menu.acessar_historico

  rescue => ex
    raise "100 - FALHA NO LOGIN SIGNUS - USER: #{user} PASS: #{pass}" if ex.message.include? "FALHA NO LOGIN SIGNUS"
    raise "102 - FALHA NO ACESSO <#{ex.message}>"
  end
  #######################################################################################
  # PERCORRE CHAMADOS P/ OBTER DADOS
  #######################################################################################
  $log.info "PERCORRE CHAMADOS P/ OBTER DADOS"
  contador_ocorrencia = 1
  #$chamados = ["P6024413"] 
  #$chamados.reverse!    
  $chamados.each do |num_ocorrencia|
    #array_fila_updates = Array.new

    $log.info "################################################################################"
    $log.info "INICIO DO ATENDIMENTO CHAMADO: #{num_ocorrencia} - #{contador_ocorrencia}/#{total_chamados} - #{Time.now}"
    
    next if historico.informar_chamado(num_ocorrencia) == false
    
    #email.enviar_email_operacao(lista_email_from, lista_email_to, "ROBO LILI EMAIL AUTOMATICO - PROCESSO DE ATUALIZAR FILA/FECHAMENTO NO SPREADUP", "CHAMADO :#{num_ocorrencia} - NAO ENCONTRADO NO SIGNUS")

    status_ocorrencia = historico.retorna_status_ocorrencia_historico(1)
    
    $log.info "#{status_ocorrencia}"
    
    historico.acessar_ocorrencia(1)
    
    historico.grid_selecao_historico(1).set 's'
    teclar_enter
    pecas = Array.new
    requisicao_post_hash = Hash.new
    pecas = historico.obter_texto_livre_F12
     
    # => CHAMADO SEM INFORMAÇÕES DA TELA F12 OU INCORRETAS     
    if (pecas == nil || pecas == []) && status_ocorrencia != "F"      
        $log.info "CHAMADO: #{num_ocorrencia} - ENVIAR EMAIL CHAMADO COM ERRO"
        email.enviar_email($lista_email_from, $lista_email_to, "ROBO PECANHA - ERRO TELA F12", "CHAMADO SIGNUS: #{num_ocorrencia} SEM DESCRITIVO DE PECAS OU DESPADRONIZADAS")
        historico.retornar_para_pesquisa
        next      
    else
      pecas = ["|||"] if (pecas == nil || pecas == [])

      $log.info "CHAMADO: #{num_ocorrencia} com historico"
      $log.info "#{pecas}"     

      requisicao_post_json = JSON.generate(
        "NumeroChamadoDoCliente"=>num_ocorrencia.to_s,
        "Pecas"=>pecas
      )
      
      if api_spreadup.api_atualizachamados_post(requisicao_post_json)
        $log.info "CHAMADO #{num_ocorrencia} ATUALIZADO COM SUCESSO NO SPREADUP"        
      else
        #UPDATE BANCO DE DADOS DE CONTROLE DE FILAS EM CASO DE ERRO DA API DE UPDATE        
        $log.info "ERRO NA ATUALIZACAO DA FILA DE OCORRENCIAS DO CHAMADO #{num_ocorrencia}"
      end      
    end
    rescue Net::ReadTimeout
      $log.info "ERRO: ROBO NÃO RECEBEU RESPOSTA DA API SpreaUP"
      $log.info "#{ex}"
    ensure
      historico.retornar_para_pesquisa
      contador_ocorrencia += 1
    end
  
    $log.info "FIM DA FILA DE CHAMADOS"  

rescue => ex
  # lista_email_from = Configuracao["lista_email_from"]
  # lista_email_to = Configuracao["lista_email_to"]
  $browser.close if !$browser.nil?
  

  #ERRO 100 - FALHA NO LOGIN##########################################################
  if ex.message.include? "100"
    email.enviar_email($lista_email_from, $lista_email_to, "ROBO PECANHA", "FOI NECESSARIO PARAR O ROBO: --Mensagem #{ex.message}")	
    raise "#{ex.message}"        
  else
    #email.enviar_email(lista_email_from, lista_email_to, "ROBO LILI PROCESSO ATUALIZA FILA E FECHAMENTO SPREADUP", "ERRO NAO MAPEADO - MENSAGEM DO ERRO: #{ex.message}") if Time.now.hour.to_s != "20"
    #20HORAS SIGNUS EM MANUTENCAO############################################
    sleep 600 if Time.now.hour.to_s == "20" # => PAUSA A EXECUÇÃO POR 10 MIN - 20HORAS O SIGNUS ENTRA EM MANUTENÇÃO
    $log.info "ERRO NAO MAPEADO: #{ex.message}"
    #retry
  end
  ####################################################################################  
end