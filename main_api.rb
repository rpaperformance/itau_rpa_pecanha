﻿#encoding: utf-8
####################################################################
# PROJETO.: RPA - PEÇANHA
# AUTOR...: DOUGLAS PASSOS SANTOS
# EMPRESA.: SPREAD
####################################################################
require "./sistema/support/env"


api_spreadup = Servico_spreadup.new
#data_base = Data_base.new


num_ocorrencia = "P6014836"
pecas = ["|01|ALCI0080 - PLACA APA 8 - 3EH73031AE|", "|01|ALCN0229 - PLACA UAI8 3EH73005AC|", "|01|ALCN0229 - PLACA UAI8 3EH73005AC|"]


      #api_atualizachamados_post
      pecas_hash = {
        NumeroChamadoDoCliente: num_ocorrencia,
        Pecas: pecas
      }

     json = JSON.generate(
       "NumeroChamadoDoCliente"=>num_ocorrencia.to_s,
       "Pecas"=>pecas
     )   
     
     puts json

     #puts JSON.parse json

      if api_spreadup.api_atualizachamados_post(json)
        $log.info "CHAMADO #{num_ocorrencia} ATUALIZADO COM SUCESSO NO SPREADUP"        
      else
        #UPDATE BANCO DE DADOS DE CONTROLE DE FILAS EM CASO DE ERRO DA API DE UPDATE        
        $log.info "ERRO NA ATUALIZACAO DA FILA DE OCORRENCIAS DO CHAMADO #{num_ocorrencia}"
      end

  
  

 
  ####################################################################################  