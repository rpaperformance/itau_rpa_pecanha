############################################################################################
### Common Locators                                                                      ###
############################################################################################

class CommonLocators
    ############################################################################################
    ### Locators Acesso Signus                                                               ###
    ############################################################################################
    def campo_signus
        $browser.text_field(id: 'Input_0_3_2')
    end

    def txt_usuario
        $browser.text_field(id: 'Input_0_10_44') 
    end

    def txt_senha
        $browser.text_field(id: 'Input_0_12_44') 
    end

    def txt_transacao
        $browser.text_field(id: 'Input_0_1_1')
    end

    def msg_rodape
        begin
             retries ||= 0
            $browser.span(id: 'protected_24_2')
        rescue
            sleep 1
            retry if (retries += 1 ) < 3
        end
    end

    def msg_login
        $browser.span(id: 'protected_23_2')
    end
    
end