require './sistema/pages/common_locators'

class CommonPage < CommonLocators
############################################################################################
### Acesso ao Signus
############################################################################################
    def acessar_signus(link, sistema)
        p "metodo acessar signus"
        abrir_navegador        
        $browser.goto link
        #$browser = $browser.form(id: 'H2W_Form')
        campo_signus.set sistema 
        teclar_enter

        #incluir tratamento caso o acesso ao sistema falhe

    end

    def fazer_login(user, pass)
        txt_usuario.set user
        txt_senha.set pass
        teclar_enter
        sleep 1
        if msg_login.exists?
            return msg_login.text
        else
            return "SUCESSO"
        end


    end

    def acessar_transacao(transacao)
        txt_transacao.set transacao
        teclar_enter
    end 

end