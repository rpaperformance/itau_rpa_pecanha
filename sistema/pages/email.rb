#require 'net/smtp'

class Email

  # Metodo para enviar emails automaticos
  def enviar_email(email_to, email_from, assunto, mensagem)
    $log.info "ENVIANDO EMAIL"
    $log.info "ASSUNTO: #{assunto}"
    options = {:address => "10.10.10.40",
               :port => 25,
               :domain => 'spread.com.br',
               :enable_starttls_auto => true}

    @mail = Mail.deliver do
      delivery_method :smtp, options
      from email_to
      to email_from
      subject assunto

      html_part do
        content_type 'text/html; charset=UTF-8'
        body "<p>#{mensagem}<p>"
      end
    end
  end

  def enviar_email_operacao(email_to, email_from, assunto, mensagem)
    options = {:address => "10.10.10.40",
               :port => 25,
               :domain => 'spread.com.br',
               :enable_starttls_auto => true}

    @mail = Mail.deliver do
      delivery_method :smtp, options
      from email_from
      to email_to
      subject assunto

      html_part do
        content_type 'text/html; charset=UTF-8'
        body "<p>#{mensagem}<p>"
      end
    end
  end


end
