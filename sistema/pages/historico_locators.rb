class HistoricoLocators

  def txt_ocorrencia
    $browser.text_field(id: 'Input_0_4_20')
  end

  def txt_equipamento
    $browser.text_field(id: 'Input_0_7_20')
  end

  def txt_cod_sintoma
    $browser.text_field(id: 'Input_0_9_20')
  end

  def txt_area_responsavel
    $browser.text_field(id: 'Input_0_13_20')
  end

  def txt_status
    $browser.text_field(id: 'Input_0_22_48')
  end

  def txt_painel
    $browser.text_field(id: 'Input_0_23_48')
  end

  def txt_fases
    $browser.text_field(id: 'Input_0_22_63')
  end

  def sintoma_painel_k
    $browser.span(id: "protected_7_7")
  end

  def txt_qtd_registro
    $browser.span(id: 'protected_3_76')
  end

  def txt_qtd_pagina
    $browser.span(id: 'protected_3_70')
  end

  def txt_pagina_atual
    $browser.span(id: 'protected_3_64')
  end

  def txt_retorna_cod_ocorrencia_historico(incrementador)
    $browser.span(id: "protected_#{incrementador + 6}_7")
  end

  def input_grid_selecao_historico(incrementador)
    $browser.text_field(id: "Input_0_#{incrementador + 6}_3")
  end

  def txt_nome_tela
    $browser.span(id: 'protected_2_25')
  end

  def nome_tela
    begin
      retries ||= 0
      $browser.span(id: 'protected_2_25').text
    rescue
      sleep 1
      retry if (retries += 1) < 3
    end
  end

  def txt_ocorrencia_info(incrementador)
    $browser.span(id: "protected_#{incrementador + 6}_7")
  end

  def nome_tela_locator
    begin
      retries ||= 0
      $browser.span(id: 'protected_2_25')
    rescue
      sleep 1
      retry if (retries += 1) < 3
    end
  end

  def nome_tela_historico
    begin
      retries ||= 0
      $browser.span(id: 'protected_2_24').text
    rescue
      sleep 1
      retry if (retries += 1) < 3
    end
  end

  def msg_rodape_historico
    $browser.span(id: 'protected_24_2')
  end

  def txt_sintoma
    begin
      retries ||= 0
      return $browser.span(id: 'protected_17_16').text + ' - ' + $browser.span(id: 'protected_17_22').text if $browser.span(id: 'protected_17_16').exists?
      return $browser.span(id: 'protected_17_19').text + ' - ' + $browser.span(id: 'protected_17_25').text.strip
    rescue
      sleep 1
      retry if (retries += 1) < 2
    end
  end

  def txt_solucao
    begin
      retries ||= 0
      # return $browser.span(id: 'protected_17_16').text + ' - ' + $browser.span(id: 'protected_17_22').text if $browser.span(id: 'protected_17_16').exists?
      # return $browser.span(id: 'protected_18_19').text + ' - ' + $browser.span(id: 'protected_18_25').text.strip if $browser.span(id: 'protected_18_19').exists?
      if $browser.span(id: 'protected_18_19').exists?
        $browser.span(id: 'protected_18_19').text + ' - ' + $browser.span(id: 'protected_18_25').text.strip
      end
    rescue
      sleep 1
      retry if (retries += 1) < 2
    end
  end

  def msg_rodape
    begin
      retries ||= 0
      $browser.span(id: 'protected_24_2')
    rescue
      sleep 1
      retry if (retries += 1) < 2
    end
  end

  def txt_cluster
    $browser.span(id: 'protected_4_53')
  end

  def txt_linhas_texto_livre(incrementador)
    if $browser.span(id: "protected_#{incrementador + 5}_7").exists?
      return $browser.span(id: "protected_#{incrementador + 5}_7").text
    elsif $browser.a(class: "hpmenu protected_blue").exists?
      return $browser.a(class: "hpmenu protected_blue").text
    else
      return ""
    end
  end

  def txt_textolivre_linha_1
    $browser.span(id: 'protected_6_7')
  end

  def txt_textolivre_linha_2
    $browser.span(id: 'protected_7_7')
  end

  def txt_textolivre_linha_3
    $browser.span(id: 'protected_8_7')
  end

  def txt_textolivre_linha_4
    $browser.span(id: 'protected_9_7')
  end

  def txt_textolivre_linha_5
    $browser.span(id: 'protected_10_7')
  end

  def txt_textolivre_linha_6
    $browser.span(id: 'protected_11_7')
  end

  def txt_textolivre_linha_7
    $browser.span(id: 'protected_12_7')
  end

  def txt_textolivre_linha_8
    $browser.span(id: 'protected_13_7')
  end

  def txt_textolivre_linha_9
    $browser.span(id: 'protected_14_7')
  end

  def txt_textolivre_linha_10
    $browser.span(id: 'protected_15_7')
  end

  def txt_textolivre_linha_11
    $browser.span(id: 'protected_16_7')
  end

  def txt_textolivre_linha_12
    $browser.span(id: 'protected_17_7')
  end

  def txt_textolivre_linha_13
    $browser.span(id: 'protected_18_7')
  end
end