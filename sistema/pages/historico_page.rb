require "./sistema/pages/historico_locators"

class HistoricoPage < HistoricoLocators
  # Preenche os campos para pesquisa de chamados
  def informar_chamado(ocorrencia)
    $log.info "INFORMANDO OCORRENCIA: #{ocorrencia}"
    limpar_campos_historico
    #sleep 0.5
    #puts "102 - FALHA AO OBTER NUMERO DA OCORRENCIA NO MOMENTO DO PREENCHIMENTO (HistoricoPage.informar_chamado)" if ocorrencia.nil?
    retornar_para_pesquisa
    return false if ocorrencia.nil?
    txt_ocorrencia.set ocorrencia #"P0945758"
    txt_painel.set "K"
    txt_fases.set "S"
    txt_status.set "F"
    teclar_enter
    sleep 0.5
    if msg_rodape_historico.exists?
      $log.info "ERRO NO CHAMADO: #{ocorrencia} - #{msg_rodape_historico.text}"
      return false
    end
    return true
  end

  def limpar_campos_historico
    $browser.send_keys :f6
  end

  def qtd_registro
    txt_qtd_registro.text.to_i
  end

  def qtd_pagina
    txt_qtd_pagina.text.to_i
  end

  def pagina_atual
    txt_pagina_atual.text.to_i
  end

  #Seleciona chamado corrente(incrementador => valor passado pelo <cont> do main)
  # def selecionar_item_historico(incrementador)
  #   sleep 1
  #   $cod_reg_ocor = retorna_cod_ocorrencia_historico(incrementador) # => Captura o codigo da ocorrencia (Número do chamado)
  #   input_grid_selecao_historico(incrementador).set 's' # Preenche um <s> na frente do chamado
  #   teclar_enter # Acessa o chamado
  # end

  def acessar_ocorrencia(incrementador)
    sleep 0.5
    #$sintoma_ocorrencia = sintoma_painel_k.text[30..32]
    input_grid_selecao_historico(incrementador).set 's' # Preenche um <s> na frente do chamado
    teclar_enter # Acessa o chamado
  end

  # Retorna o número do chamado
  def retorna_cod_ocorrencia_historico(incrementador)
    txt_retorna_cod_ocorrencia_historico(incrementador).text[0, 8]
  end

  def retorna_status_ocorrencia_historico(incrementador)
    index = txt_retorna_cod_ocorrencia_historico(incrementador).text.strip.length
    return txt_retorna_cod_ocorrencia_historico(incrementador).text.strip[index-1]
    #return txt_retorna_cod_ocorrencia_historico(incrementador).strip.text[0, 8]
  end

  def captura_fila_ocorrencia
    $log.info "CAPTURA DE FILA DE OCORRENCIAS"
    sleep 0.5
    return false if txt_cluster.exists?

    $array_fila_ocorrencia = Array.new
    hash_item_fila = Hash.new
    cont_pag = 1
    paginas = qtd_pagina
    $index = qtd_registro
    cont_reg_pag = 1 # Atualiza para último registro da pagina
    $array_areas = Array.new
    qtd_recorrencias_atdspread = 0
    $data_hora_fechamento = ""

    cont = 1
    while cont_reg_pag <= $index
      hash_item_fila = Hash.new
      $str_texto_livre = ""
      data_hora_transferencia = ocorrencia_info(cont)[16..34]
      data_hora_transferencia = "#{data_hora_transferencia[6..9]}-#{data_hora_transferencia[3..4]}-#{data_hora_transferencia[0..1]} #{data_hora_transferencia[11..20].gsub('.', ':')}"
      hash_item_fila['Fila'] = ocorrencia_info(cont)[56..63]
      hash_item_fila['DataDeTransferencia'] = data_hora_transferencia
      $array_fila_ocorrencia << hash_item_fila
      if cont == 13 and cont_reg_pag < $index
        proxima_pagina
        cont = 1
        sleep 0.5
      else
        cont += 1 if cont_reg_pag < $index
      end
      cont_reg_pag += 1
    end

    # if cont_reg_pag == 14
    #   cont = 13
    # else
    #   cont -= 1
    # end
    $status_ocorrencia = ocorrencia_info(cont)[69..69]
    if $status_ocorrencia == "F"
      $data_hora_fechamento = ocorrencia_info(cont)[36..54]
      $data_hora_fechamento = "#{$data_hora_fechamento[6..9]}-#{$data_hora_fechamento[3..4]}-#{$data_hora_fechamento[0..1]} #{$data_hora_fechamento[11..20].gsub('.', ':')}"
    end
    #TODO - CONTROLE DO NUMERO DE SEQUENCIA DA QUANTIDADE DE FILA
    # SE O CONTROLE DE SEQUENCIA FOR IGUAL AO DO SIGNUS, DEVE PULAR A AÇÃO.
    grid_selecao_historico(1).set 's'
    teclar_enter
    sleep 1
    $sintoma_ocorrencia = txt_sintoma
    $solucao_ocorrencia = txt_solucao
    if $solucao_ocorrencia != nil
      #retornar_texto_livre
      $str_texto_livre = obter_texto_livre
    end
    sleep 1
    $browser.send_keys :f3
    return true
  end


  def obter_texto_livre
    $log.info "CAPTURA DE INFORMACOES DA TELA DE TEXTO LIVRE - F5"
    $browser.send_keys :f5
    sleep 1
    texto = ''
    if msg_rodape.exist?
      #puts '   ' + Time.now.strftime('%Y/%m/%d - %T').to_s + 'Mensagem do sistema: <' + msg_rodape.text + '>'
      $log.info "   #{Time.now.strftime('%Y/%m/%d - %T').to_s} Mensagem do sistema: < #{msg_rodape.text} >"
      return nil if msg_rodape.exist?
    end
    @paginas = qtd_pagina

    @paginas.times do
      @linhas = 1
      while @linhas <= 13
        #texto << $browser.text_field(id: "Input_0_#{@linhas}_7").value + ' \n'
        texto << txt_linhas_texto_livre(@linhas).strip + " \n"
        @linhas += 1
      end
      @paginas == pagina_atual ? break : $browser.send_keys(:f8)
      sleep 1
    end

    $browser.send_keys :f3
    sleep 2
    return texto
  end

  def obter_texto_livre_F12
    $log.info "CAPTURA DE INFORMACOES DA TELA DE TEXTO LIVRE - F12"
    $browser.send_keys :f12
    sleep 1
    $array_linhas_pecas = Array.new
    #texto = ''
    if msg_rodape.exist?
      $log.info "   #{Time.now.strftime('%Y/%m/%d - %T').to_s} Mensagem do sistema: < #{msg_rodape.text} >"
      return nil if msg_rodape.exist?
    end
    @paginas = qtd_pagina

    @paginas.times do
      @linhas = 1
      while @linhas <= 13
        #texto << $browser.text_field(id: "Input_0_#{@linhas}_7").value + ' \n'        
        
        if txt_linhas_texto_livre(@linhas).strip.size > 0
          linha = (txt_linhas_texto_livre(@linhas).strip.to_s)
          if linha.index("|") == 0 && linha.count("|") == 3
            $array_linhas_pecas << linha
          else
            $log.info "INFORMAÇÕES DE PECAS FORA DO PADRÃO"
            return nil
          end
          
        else
          
          break
        end
        #$array_linhas_pecas << txt_linhas_texto_livre(@linhas).strip
       
        #binding.pry
        @linhas += 1
      end
      @paginas == pagina_atual ? break : $browser.send_keys(:f8)
      sleep 1
    end

    $browser.send_keys :f3
    sleep 2
    return $array_linhas_pecas
  end

  

  def retorno_metodo_linha(locator)
    if locator.text != nil || locator != ""
      $str_texto_livre = $str_texto_livre + " \r\n "
      $str_texto_livre = $str_texto_livre + locator.text
      return false
    else
      return true
    end
  end

  def retornar_para_historico
    sleep 1
    $browser.send_keys :f3
    sleep 1
    if validar_tela_pendentes_ocorrencia
      $browser.send_keys :f3
    end
  end

  def retornar_para_pesquisa
    cont = 0
    while !txt_ocorrencia.exists? and cont < 2
      $browser.send_keys :f3
      sleep 1 #=> tempo minimo validado para essa situação
      cont += 1
    end
  end

  def proxima_pagina
    $browser.send_keys :f8
    sleep 2
  end

  def registro_valido?
    ocorrencia_info(incrementador)
  end

  def ocorrencia_info(incrementador)
    txt_ocorrencia_info(incrementador).text
  end

  def grid_selecao_historico(incrementador)
    input_grid_selecao_historico(incrementador)
  end

  ############################################################################################
  ###Support Definitions                                                                   ###
  ############################################################################################
  def validar_tela_pendentes_ocorrencia
    if nome_tela_locator.exist? and nome_tela.include?('PENDENTES')
      return true
    end
    return false
  end

end




