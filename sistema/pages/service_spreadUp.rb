class Servico_spreadup

  def initialize
    $server = Configuracao["api_server"]
    $get_listachamados = Configuracao["api_listachamados"]
    $post_listachamados = Configuracao["api_atualizachamados"]
  end

  #RETORNA A LISTA DOS CHAMADOS DO SPREADUP-RETORNO == ARRAY
  def obter_lista_chamados_spreadUp

    get_chamados = HTTParty.get("#{$server}#{$get_listachamados}") #Ambiente de teste
    if get_chamados.code == 200
      return get_chamados
    else
      return 'SEM CHAMADO SPREADUP'
    end
  end

  # POST - ATUALIZA A FILA E COD DO SINTOMA DE CADA CHAMADO
  def api_atualizachamados_post(requisicao_json)
      
    url_post_update = "#{$server}#{$post_listachamados}" 
    #url_post_update = "http://192.168.229.17:8099#{$post_listachamados}"

    $log.info "API UPDATE DE PECAS"
    # body_json = requisicao_hash.to_json
    # $log.info requisicao_hash.to_json
    
    post = HTTParty.post(
        url_post_update,
        body: requisicao_json,
        headers: {'Content-Type' => 'application/json', 'Accept' => 'application/json'},
        timeout: $api_spreadup_timeout
    )    

    $log.info "CODIGO RESPOSTA API:#{post.code}"
    
    if post.code == 200      
      return true # verificar se há a necessidade do retorno boleano
    else
      $log.info "MENSAGEM BODY API:"
      $log.info "#{post.body}"
      return false # verificar se há a necessidade do retorno boleano
    end
  end

end


