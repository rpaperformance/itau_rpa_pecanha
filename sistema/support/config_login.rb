require_relative "env"

#encoding: UTF-8

require 'yaml'

class Configuracao
  def self.[] key
    @@config[key]
  end

  def self.load name
    @@config = nil
    io = File.open(File.expand_path(".", Dir.pwd) + "/sistema/config_acesso/config.yml" )
    YAML::load_stream(io) { |doc| @@config = doc[name] }
    raise "Não foi possível encontrar o nome da configuração: \"#{name}\"" unless @@config
  end

  def self.[]= key, value
    @@config[key] = value
  end
end

Configuracao.load("hml") # Dev, hml ou prod