require 'mail'
require 'watir'
require 'httparty'
#require "net/http"
#require 'rest-client' 
#require "uri" 
require 'pry'
require 'yaml'
require 'logging'
require 'json'
require 'json/ext'

require "./sistema/support/eventos_terminal.rb"
#require "./sistema/support/manipula_excel.rb"
#require "./sistema/support/libs/eventos_web.rb"
#require "./sistema/support/excel.rb"
require "./sistema/support/config_login.rb"

#Requires de pages
require "./sistema/pages/menu_page.rb"
require "./sistema/pages/common_page.rb"
require "./sistema/pages/historico_page.rb"
require "./sistema/pages/email.rb"
require "./sistema/pages/service_spreadUp.rb"

$log = Logging.logger['PECANHA - BOT']
$log.add_appenders( Logging.appenders.stdout ) 
$log.level = :info

def abrir_navegador
    $log.info "Start ENV - abrir_navegador" 
    Watir.default_timeout = Configuracao['timeout']
    $browser = Watir::Browser.new(:chrome, headless: Configuracao['headless'])
end

$api_spreadup_timeout = 180