########################################################################################################################
# Excel
########################################################################################################################

#Metodo para extração da massa marcada como não usada na planilha informada
def retorna_hash_massa_planilha(caminho_planilha)
    t1 = Thread.new{
        require 'win32ole'
        fechar_processo_excel
        abrir_planilha_excel(caminho_planilha)
        linha = retorna_linha_utilizavel.to_i
        $dados_planilha = Hash.new
        cont = 0
        troca_conversao = ""
        $ws.UsedRange.Rows(1).Value[0].each do |x|
            if is_number?($ws.UsedRange.Rows(linha).Value[0][cont])
                troca_conversao = $ws.UsedRange.Rows(linha).Value[0][cont].to_i.to_s    # <--Números são retornados no formato float(esse comando tira o '.0' do número e converte para string)
            else
                troca_conversao = $ws.UsedRange.Rows(linha).Value[0][cont]
            end
            $dados_planilha[x] = troca_conversao
            cont += 1
        end
        $excel.AlertBeforeOverwriting = false
        $excel.DisplayAlerts = false
        $excel.Save
        $excel.Quit()
        fechar_processo_excel
    }
    t1.join
end

def retorna_linha_utilizavel
linha_nao_usada = 0
coluna_status = false
    for col in 1..$ws.UsedRange.Columns.Count do
        if $ws.Cells(1,col).Value == "STATUS"
            coluna_status = true
            for row in 2..$ws.UsedRange.Rows.Count
                if $ws.Cells(row,col).Value == "disponivel"
                    linha_nao_usada = row
                    $ws.Cells(row,col).Value = "usado"
                    return linha_nao_usada
                end
            end
        end
    end
    raise "Massa de Dados - Não foi encontrada massa de dados disponível para utilização." if linha_nao_usada == 0
    raise "Massa de Dados - Não foi encontrada a coluna Status para identificar parâmetro já utilizados" if coluna_status == false
end

def abrir_planilha_excel(caminho_planilha, worksheet = 1)
    fechar_processo_excel
    $excel = WIN32OLE.new('Excel.Application')
    workbook = $excel.Workbooks.Open(caminho_planilha)
    $excel.Visible = false
    $ws = workbook.Worksheets worksheet
end

def fechar_processo_excel
    wmi = WIN32OLE.connect("winmgmts://")
    processos = wmi.ExecQuery("Select * from Win32_Process Where NAME = 'EXCEL.EXE'")
    processos.each do |processo|
        Process.kill('KILL', processo.ProcessID.to_i)
    end
    sleep 2
end

def inserir_dados_planilha(linha, coluna, valor)
    $ws.Cells(linha, coluna).value = valor
end

def salvar_planilha
    $ws.Columns.AutoFit
    #pwd =  Dir.pwd.gsub('/','\\') << '\\'
    pwd = $path_save_sheet.gsub('/','\\') << '\\'
    if $cod_sintoma == '' or $cod_sintoma == nil
        $wb.SaveAs("#{pwd}#{Time.now.to_s[0..18].gsub(':', '_')}_#{$area.gsub("%","")}_Signus_Out")
    else
        $wb.SaveAs("#{pwd}#{Time.now.to_s[0..18].gsub(':', '_')}_#{$area.gsub("%","")}_#{$cod_sintoma}_Signus_Out")
    end
    $xl.Quit()
end
