


class Excel
    
    def fechar_processo_excel
        wmi = WIN32OLE.connect("winmgmts://") 
        processos = wmi.ExecQuery("Select * from Win32_Process Where NAME = 'EXCEL.EXE'")
        processos.each do |processo| 
        Process.kill('KILL', processo.ProcessID.to_i)
        end 
        sleep 2
    end

    def abrir_planilha(caminho_planilha, worksheet = 1)
        fechar_processo_excel
        excel = WIN32OLE.new('Excel.Application')
        workbook = excel.Workbooks.Open(caminho_planilha)
        #excel.Visible = false
        $ws = workbook.Worksheets worksheet
    end

    def criar_planilha_excel
        $xl = WIN32OLE.new('Excel.Application')
        #$xl.Visible = true
        $wb = $xl.Workbooks.Add
        $ws = $wb.Worksheets 1
        $ws.name = "Global"

        $ws.Range("A1:L1").value = ["N_Ocorrencia", "Agencia", "DT_HR_Abertura", "DT_HR_Transferencia", "Area_Responsavel", "Sintoma", "Cod_Segmento","Segmento", "NomeSolicitante", "Observacao", "DescricaoInterna", "TempoExtracao"]
        $ws.Columns.AutoFit
        $ws.Columns(2).NumberFormat = "0000"
    end

    def inserir_dados_planilha(linha, coluna, valor)
        $ws.Cells(linha, coluna).value = valor
    end

    def salvar_planilha
        $ws.Columns.AutoFit
        #pwd =  Dir.pwd.gsub('/','\\') << '\\' #POG - Para funcionar o SaveAs, precisa substituir as barras
        pwd = $path_save_sheet.gsub('/','\\') << '\\'
        if $cod_sintoma == '' or $cod_sintoma == nil
            $wb.SaveAs("#{pwd}#{Time.now.to_s[0..18].gsub(':', '_')}_#{$area.gsub("%","")}_Signus_Out")
        else
            $wb.SaveAs("#{pwd}#{Time.now.to_s[0..18].gsub(':', '_')}_#{$area.gsub("%","")}_#{$cod_sintoma}_Signus_Out")
        end
        
        $xl.Quit()
    end
    
    
    def obter_linha_vazia
        linha = 2
        loop do
            break if $ws.Cells(linha, 1).value.equal? nil
            linha += 1
        end
        linha
    end

end